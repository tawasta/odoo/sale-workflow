# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sale_order_description
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 17.0-20240126\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-29 12:42+0000\n"
"PO-Revision-Date: 2024-07-29 15:43+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"X-Generator: Poedit 3.0.1\n"

#. module: sale_order_description
#: model_terms:ir.ui.view,arch_db:sale_order_description.view_order_form
msgid "Add internal notes..."
msgstr "Lisää sisäisiä muistiinpanoja..."

#. module: sale_order_description
#: model:ir.model.fields,field_description:sale_order_description.field_sale_order__description
#: model_terms:ir.ui.view,arch_db:sale_order_description.view_order_form
msgid "Description"
msgstr "Kuvaus"

#. module: sale_order_description
#: model:ir.model.fields,help:sale_order_description.field_sale_order__description
msgid "Internal notes"
msgstr "Sisäiset muistiinpanot"

#. module: sale_order_description
#: model:ir.model,name:sale_order_description.model_sale_order
msgid "Sales Order"
msgstr "Myyntitilaus"
